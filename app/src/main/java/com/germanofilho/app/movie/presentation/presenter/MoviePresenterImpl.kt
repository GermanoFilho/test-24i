package com.germanofilho.app.movie.presentation.presenter

import android.util.Log
import com.germanofilho.app.core.presenter.BasePresenterImpl
import com.germanofilho.app.movie.model.usecase.MovieUseCase
import com.germanofilho.app.movie.presentation.view.MovieView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class MoviePresenterImpl @Inject constructor(private val mView: MovieView) : BasePresenterImpl(mView), MoviePresenter {

    @Inject
    lateinit var mUseCase: MovieUseCase

    override fun fetchMovieList(endDate : String) {
        mView.isToShowProgressBar(true)
        val observable = execute(mUseCase.getMovieList(endDate))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.isToShowProgressBar(false)
                    mView.loadRecyclerView(it.results.filter { r -> !r.adult })

                }, {
                    mView.isToShowProgressBar(false)
                    Log.e("Service", it.message)
                })

        mCompositeDisposable.add(observable)
    }

    override fun fetchMovieImage(movieId: Int, position: Int)  {
        val observable = execute(mUseCase.getMovieImage(movieId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.retrieveMovieImage(it.posters.last().urlPoster!!, position)
                }, {
                    Log.e("Service", it.message)
                })

        mCompositeDisposable.add(observable)
    }
}