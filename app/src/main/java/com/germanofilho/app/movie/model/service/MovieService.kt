package com.germanofilho.app.movie.model.service

import com.germanofilho.app.core.model.domain.entity.MovieImageResponse
import com.germanofilho.app.core.model.domain.entity.MovieResponse
import com.germanofilho.app.core.model.service.BaseService
import io.reactivex.Observable

interface MovieService : BaseService {
    fun getMovieList(endDate: String) : Observable<MovieResponse>
    fun getMovieImage(movieId: Int) : Observable<MovieImageResponse>
}