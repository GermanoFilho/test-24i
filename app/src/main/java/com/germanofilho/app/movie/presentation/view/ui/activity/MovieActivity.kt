package com.germanofilho.app.movie.presentation.view.ui.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.germanofilho.app.App
import com.germanofilho.app.R
import com.germanofilho.app.core.model.domain.entity.Movie
import com.germanofilho.app.core.util.ScreenUtil
import com.germanofilho.app.core.view.ui.activity.BaseActivity
import com.germanofilho.app.movie.di.MovieModule
import com.germanofilho.app.movie.presentation.presenter.MoviePresenter
import com.germanofilho.app.movie.presentation.view.MovieView
import com.germanofilho.app.movie.presentation.view.ui.adapter.MovieAdapter
import kotlinx.android.synthetic.main.activity_movie.*
import kotlinx.android.synthetic.main.movie_toolbar.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class MovieActivity : BaseActivity<MoviePresenter>(), MovieView {

    @Inject
    lateinit var mPresenter : MoviePresenter

    lateinit var mAdapter: MovieAdapter

    private var selectedNumber : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        App.get(this)
                .getAppComponent()
                .plus(MovieModule(this))
                .inject(this)

        isTablet()
        loadSpinner()
        mPresenter.fetchMovieList(convertToDate(0))
    }

    override fun onStart() {
        super.onStart()
        iv_end_date.setOnClickListener {
            if(layout_select_end_date.isShown){
                layout_select_end_date.visibility = View.GONE
            } else {
                layout_select_end_date.visibility = View.VISIBLE
            }
        }

        btn_ok.setOnClickListener {
            recycler_view_movie_list.visibility = View.GONE
            mPresenter.fetchMovieList(convertToDate(selectedNumber))
        }
    }

    override fun loadRecyclerView(movies: List<Movie>) {
        recycler_view_movie_list.visibility = View.VISIBLE
        val layoutManager = GridLayoutManager(applicationContext, ScreenUtil.setValueIfTablet())
        recycler_view_movie_list.layoutManager = layoutManager
        recycler_view_movie_list.itemAnimator = null
        mAdapter = MovieAdapter(this, movies, mPresenter)
        recycler_view_movie_list.adapter = mAdapter
    }

    override fun retrieveMovieImage(url : String, position: Int){
        mAdapter.setMovieUrl(url, position)
    }

    private fun loadSpinner(){
        val endDays = arrayOf("1 day", "2 days", "3 days", "4 days", "5 days", "6 days",
                "7 days", "8 days", "9 days", "10 days", "11 days", "12 days", "13 days", "14 days")

        spn_days.adapter = ArrayAdapter(this, R.layout.spinner, endDays)
        spn_days.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedNumber = p0!!.selectedItem.toString().split(" ").first().toInt()
            }
        }
    }

    private fun convertToDate(number: Int) : String{
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -number)
        return sdf.format(cal.time)
    }
}
