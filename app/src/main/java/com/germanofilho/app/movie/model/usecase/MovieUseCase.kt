package com.germanofilho.app.movie.model.usecase

import com.germanofilho.app.core.model.domain.entity.MovieImageResponse
import com.germanofilho.app.core.model.domain.entity.MovieResponse
import io.reactivex.Observable

interface MovieUseCase{
    fun getMovieList(endDate : String) : Observable<MovieResponse>
    fun getMovieImage(movieId : Int) : Observable<MovieImageResponse>
}