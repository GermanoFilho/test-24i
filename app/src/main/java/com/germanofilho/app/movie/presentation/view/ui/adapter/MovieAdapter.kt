package com.germanofilho.app.movie.presentation.view.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.germanofilho.app.BuildConfig
import com.germanofilho.app.R
import com.germanofilho.app.core.model.domain.entity.Movie
import com.germanofilho.app.core.util.ScreenUtil
import com.germanofilho.app.detail.presentation.view.ui.activity.DetailActivity
import com.germanofilho.app.movie.presentation.presenter.MoviePresenter
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_movie_item.view.*
import java.lang.Exception

class MovieAdapter(private val context: Context, private val movies: List<Movie>, private val presenter : MoviePresenter) :
        RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMovie(movies[position])
    }

    override fun getItemCount() = movies.size

    inner class ViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView) {

        fun bindMovie(movie: Movie) {
            Picasso.get()
                    .load(BuildConfig.BASE_IMAGE_URL.plus(ScreenUtil.getUrlTypePath()).plus(movie.url))
                    .placeholder(R.drawable.placeholder)
                    .into(containerView.img_movie_thumb,object : Callback {
                        override fun onError(e: Exception?) {
                            if(!movie.alreadyLoaded) {
                                presenter.fetchMovieImage(movie.id, adapterPosition)
                                movie.alreadyLoaded = true
                            }
                        }
                        override fun onSuccess() {}
                    })
            containerView.img_movie_thumb.setOnClickListener(onItemClickListener(movie.id))
        }
    }

    private fun onItemClickListener(id : Int): View.OnClickListener {
        return View.OnClickListener {
            context.startActivity(DetailActivity.newInstance(context, id))
        }
    }

    fun setMovieUrl(url: String, position: Int) {
        movies[position].url = url
        notifyItemChanged(position)
    }
}