package com.germanofilho.app.movie.di

import com.germanofilho.app.core.di.AppScope
import com.germanofilho.app.core.view.BaseView
import com.germanofilho.app.movie.presentation.presenter.MoviePresenter
import com.germanofilho.app.movie.presentation.presenter.MoviePresenterImpl
import com.germanofilho.app.movie.presentation.view.MovieView
import dagger.Module
import dagger.Provides

@Module
class MovieModule constructor(var view: BaseView) {

    @Provides
    @AppScope
    fun provideMovieView(): MovieView = view as MovieView

    @Provides
    @AppScope
    fun provideMovieListFragmentPresenter(presenter: MoviePresenterImpl): MoviePresenter = presenter

}