package com.germanofilho.app.movie.model.service

import com.germanofilho.app.core.model.domain.entity.MovieImageResponse
import com.germanofilho.app.core.model.domain.entity.MovieResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface MovieServiceEndPoint {
    @GET("changes")
    fun getMovies(@Query("api_key") apiKey: String, @Query("page") page : Int, @Query("endDate") endDate : String) : Observable<MovieResponse>

    @GET("{movieId}/images")
    fun getMovieImage(@Path("movieId") movieId : Int, @Query("api_key") apiKey: String) : Observable<MovieImageResponse>

}