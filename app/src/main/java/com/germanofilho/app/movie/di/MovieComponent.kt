package com.germanofilho.app.movie.di

import com.germanofilho.app.core.di.AppScope
import com.germanofilho.app.movie.presentation.view.ui.activity.MovieActivity
import dagger.Subcomponent

@AppScope
@Subcomponent(modules = [MovieModule::class])
interface MovieComponent {
    fun inject(activity : MovieActivity)
}