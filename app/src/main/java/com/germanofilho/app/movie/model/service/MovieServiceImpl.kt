package com.germanofilho.app.movie.model.service

import com.germanofilho.app.core.model.domain.entity.MovieImageResponse
import com.germanofilho.app.core.model.domain.entity.MovieResponse
import com.germanofilho.app.core.model.service.BaseServiceConnector
import com.germanofilho.app.core.model.service.BaseServiceImpl
import io.reactivex.Observable
import javax.inject.Inject

class MovieServiceImpl @Inject constructor(var retrofit : BaseServiceConnector): BaseServiceImpl(), MovieService{
    override fun getMovieList(endDate : String): Observable<MovieResponse> = retrofit.request(getBaseUrl(), MovieServiceEndPoint::class.java).getMovies(getKey(), 1, endDate)

    override fun getMovieImage(movieId: Int): Observable<MovieImageResponse> = retrofit.request(getBaseUrl(), MovieServiceEndPoint::class.java).getMovieImage(movieId, getKey())
}