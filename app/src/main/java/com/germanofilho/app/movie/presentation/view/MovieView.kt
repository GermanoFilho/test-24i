package com.germanofilho.app.movie.presentation.view

import com.germanofilho.app.core.model.domain.entity.Movie
import com.germanofilho.app.core.view.BaseView

interface MovieView : BaseView{
    fun loadRecyclerView(movies : List<Movie>)
    fun retrieveMovieImage(url : String, position: Int)
}
