package com.germanofilho.app.movie.presentation.presenter

import com.germanofilho.app.core.presenter.BasePresenter

interface MoviePresenter : BasePresenter {
    fun fetchMovieList(endDate : String)
    fun fetchMovieImage(movieId: Int, position: Int)
}