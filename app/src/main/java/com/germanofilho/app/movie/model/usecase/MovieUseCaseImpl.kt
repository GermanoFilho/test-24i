package com.germanofilho.app.movie.model.usecase

import com.germanofilho.app.core.model.domain.entity.MovieImageResponse
import com.germanofilho.app.core.model.domain.entity.MovieResponse
import com.germanofilho.app.core.model.usecase.BaseUseCaseImpl
import com.germanofilho.app.movie.model.service.MovieService
import io.reactivex.Observable
import javax.inject.Inject

class MovieUseCaseImpl @Inject constructor(private var service : MovieService) : BaseUseCaseImpl(service), MovieUseCase{

    override fun getMovieList(endDate : String): Observable<MovieResponse> = execute(service.getMovieList(endDate))

    override fun getMovieImage(movieId: Int): Observable<MovieImageResponse> = execute(service.getMovieImage(movieId))
}