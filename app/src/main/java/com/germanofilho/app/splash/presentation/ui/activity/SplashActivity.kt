package com.germanofilho.app.splash.presentation.ui.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.germanofilho.app.R
import com.germanofilho.app.core.util.AnimationUtil
import com.germanofilho.app.core.util.ScreenUtil
import com.germanofilho.app.movie.presentation.view.ui.activity.MovieActivity
import io.reactivex.Completable
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if(ScreenUtil.isTabletDevice(this)) requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE

        AnimationUtil.fadeIn(iv_logo, 1000)

        Completable.complete()
                .delay(2, TimeUnit.SECONDS)
                .doOnComplete{
                    startActivity(Intent(this, MovieActivity::class.java))
                    finishAffinity()
                }
                .subscribe()
    }
}
