package com.germanofilho.app

import android.app.Application
import android.content.Context
import com.germanofilho.app.core.di.component.AppComponent
import com.germanofilho.app.core.di.component.DaggerAppComponent
import com.germanofilho.app.core.di.module.AppModule
import com.germanofilho.app.core.di.module.NetworkModule

open class App : Application() {

    private lateinit var mAppComponent: AppComponent

    companion object {
        lateinit var instance: App
            private set

        fun get(context: Context): App = context.applicationContext as App

    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        initDagger()
    }

    fun getAppComponent(): AppComponent = mAppComponent

    open fun initDagger() {
        mAppComponent = DaggerAppComponent
                .builder()
                .build()
    }
}