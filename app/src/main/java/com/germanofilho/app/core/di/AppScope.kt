package com.germanofilho.app.core.di

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(value = AnnotationRetention.RUNTIME)
annotation class AppScope