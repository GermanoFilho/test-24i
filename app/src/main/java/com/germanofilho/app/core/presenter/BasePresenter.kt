package com.germanofilho.app.core.presenter

import io.reactivex.Observable


interface BasePresenter {

    fun <T> execute(observable : Observable<T>): Observable<T>
    fun onStop()
}