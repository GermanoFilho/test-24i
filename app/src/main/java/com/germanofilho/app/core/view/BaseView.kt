package com.germanofilho.app.core.view

interface BaseView {

    fun isToShowProgressBar(value: Boolean)

    interface Activity : BaseView {
        fun isTablet()
    }
}