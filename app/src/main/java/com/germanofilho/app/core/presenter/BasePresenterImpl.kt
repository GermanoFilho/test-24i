package com.germanofilho.app.core.presenter

import com.germanofilho.app.core.view.BaseView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BasePresenterImpl @Inject constructor(private val baseView: BaseView): BasePresenter{

    var mCompositeDisposable = CompositeDisposable()

    override fun <T> execute(observable: Observable<T>): Observable<T> {
        return observable
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext { throwable: Throwable ->
                    return@onErrorResumeNext Observable.error(throwable)
                }
    }

    override fun onStop() {
        mCompositeDisposable.clear()
    }
}