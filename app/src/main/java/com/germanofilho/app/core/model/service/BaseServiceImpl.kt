package com.germanofilho.app.core.model.service

import com.germanofilho.app.BuildConfig
import javax.inject.Singleton

@Singleton
abstract class BaseServiceImpl : BaseService {

    companion object {

        fun getBaseUrl(): String {
            return BuildConfig.BASE_URL
        }

        fun getKey(): String{
            return BuildConfig.KEY
        }

    }

}