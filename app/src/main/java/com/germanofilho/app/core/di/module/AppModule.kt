package com.germanofilho.app.core.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, UseCaseModule::class, ServiceModule::class])
open class AppModule constructor(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplication() = application
}