package com.germanofilho.app.core.model.domain.entity

import java.io.Serializable

data class Movie(var id: Int, var adult : Boolean, var url: String, var alreadyLoaded : Boolean) : Serializable