package com.germanofilho.app.core.model.usecase

import android.util.Log
import com.germanofilho.app.core.model.service.BaseService
import io.reactivex.Observable
import javax.inject.Inject

open class BaseUseCaseImpl @Inject constructor(val baseService: BaseService) : BaseUseCase {

    override fun <T> execute(observable: Observable<T>): Observable<T> {
        return observable
                .onErrorResumeNext { throwable: Throwable ->
                    Log.e("Service", throwable.message)
                    return@onErrorResumeNext observable
                }
    }

}