package com.germanofilho.app.core.model.usecase

import io.reactivex.Observable

interface BaseUseCase {

    fun <T> execute( observable: Observable<T>): Observable<T>
}