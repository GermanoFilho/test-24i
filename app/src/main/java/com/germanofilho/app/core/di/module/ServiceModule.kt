package com.germanofilho.app.core.di.module

import com.germanofilho.app.core.model.service.BaseService
import com.germanofilho.app.core.model.service.BaseServiceImpl
import com.germanofilho.app.detail.model.service.DetailService
import com.germanofilho.app.detail.model.service.DetailServiceImpl
import com.germanofilho.app.movie.model.service.MovieService
import com.germanofilho.app.movie.model.service.MovieServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideBaseService(service: BaseServiceImpl): BaseService = service

    @Provides
    @Singleton
    fun provideMovieService(service: MovieServiceImpl): MovieService = service

    @Provides
    @Singleton
    fun provideDetailService(service: DetailServiceImpl): DetailService = service
}