package com.germanofilho.app.core.model.domain.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Image(@SerializedName("file_path") var urlPoster: String?) : Serializable