package com.germanofilho.app.core.di.module

import com.germanofilho.app.core.model.usecase.BaseUseCase
import com.germanofilho.app.core.model.usecase.BaseUseCaseImpl
import com.germanofilho.app.detail.model.usecase.DetailUseCase
import com.germanofilho.app.detail.model.usecase.DetailUseCaseImpl
import com.germanofilho.app.movie.model.usecase.MovieUseCase
import com.germanofilho.app.movie.model.usecase.MovieUseCaseImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideBaseUseCase(useCase: BaseUseCaseImpl): BaseUseCase = useCase

    @Provides
    @Singleton
    fun provideMovieUseCase(useCase: MovieUseCaseImpl): MovieUseCase = useCase

    @Provides
    @Singleton
    fun provideDetailUseCase(useCase: DetailUseCaseImpl): DetailUseCase = useCase

}