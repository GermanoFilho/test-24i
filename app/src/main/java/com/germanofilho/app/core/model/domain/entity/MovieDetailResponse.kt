package com.germanofilho.app.core.model.domain.entity

import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
        @SerializedName("original_title") var title : String?,
        @SerializedName("original_language") var language : String?,
        @SerializedName("release_date") var releaseDate : String?,
        @SerializedName("poster_path") var urlPoster : String?,
        var genres : List<Genre>?,
        var overview : String?)