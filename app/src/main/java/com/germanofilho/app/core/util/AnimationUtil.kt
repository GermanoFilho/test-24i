package com.germanofilho.app.core.util

import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

open class AnimationUtil {

    companion object {
        fun fadeIn(view: View, duration: Long) {
            view.alpha = 0.2f
            view.animate().alpha(1f)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .setDuration(duration)
                    .start()
        }
    }
}