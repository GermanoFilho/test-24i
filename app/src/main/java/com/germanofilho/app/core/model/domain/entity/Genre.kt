package com.germanofilho.app.core.model.domain.entity

data class Genre(var id : Int, var name : String)