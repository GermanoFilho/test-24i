package com.germanofilho.app.core.model.domain.entity

import java.io.Serializable

data class MovieResponse(var results: List<Movie>) : Serializable