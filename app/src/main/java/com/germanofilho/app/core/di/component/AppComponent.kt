package com.germanofilho.app.core.di.component

import com.germanofilho.app.App
import com.germanofilho.app.core.di.module.AppModule
import com.germanofilho.app.core.di.module.BaseModule
import com.germanofilho.app.detail.di.DetailComponent
import com.germanofilho.app.detail.di.DetailModule
import com.germanofilho.app.movie.di.MovieComponent
import com.germanofilho.app.movie.di.MovieModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, BaseModule::class])

interface AppComponent {
    fun inject(app: App)
    fun plus(module: MovieModule): MovieComponent
    fun plus(module: DetailModule): DetailComponent
}