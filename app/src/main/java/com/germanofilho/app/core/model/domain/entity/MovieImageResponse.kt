package com.germanofilho.app.core.model.domain.entity

import java.io.Serializable

data class MovieImageResponse(var posters: List<Image>) : Serializable