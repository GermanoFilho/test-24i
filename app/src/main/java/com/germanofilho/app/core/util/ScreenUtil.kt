package com.germanofilho.app.core.util

import android.content.Context
import android.content.res.Configuration
import com.germanofilho.app.App

open class ScreenUtil {

    companion object {
        fun isTabletDevice(context : Context) : Boolean {
            return context.resources.configuration.screenLayout and
                    Configuration.SCREENLAYOUT_SIZE_MASK >=
                    Configuration.SCREENLAYOUT_SIZE_LARGE
        }

        fun setValueIfTablet() : Int {
            return if(isTabletDevice(App.instance)) 5
            else 3
        }

        fun getUrlTypePath() : String {
            return if(isTabletDevice(App.instance)) "original"
            else "w500"
        }
    }
}
