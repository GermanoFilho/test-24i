package com.germanofilho.app.core.di.module

import com.germanofilho.app.core.presenter.BasePresenter
import com.germanofilho.app.core.presenter.BasePresenterImpl
import com.germanofilho.app.core.view.BaseView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BaseModule constructor(private var baseView: BaseView) {

    @Provides
    @Singleton
    fun provideBaseView(): BaseView.Activity {
        return baseView as BaseView.Activity
    }

    @Provides
    @Singleton
    fun provideBasePresenter(presenter: BasePresenterImpl): BasePresenter {
        return presenter
    }
}