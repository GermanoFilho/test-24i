package com.germanofilho.app.detail.model.usecase

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import io.reactivex.Observable

interface DetailUseCase{
    fun getMovieDetail(movieId: Int) : Observable<MovieDetailResponse>
    fun isValidMovie(movie : MovieDetailResponse) : Boolean
}