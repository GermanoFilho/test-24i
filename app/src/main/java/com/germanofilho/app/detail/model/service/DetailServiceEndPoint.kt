package com.germanofilho.app.detail.model.service

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface DetailServiceEndPoint {

    @GET("{movieId}")
    fun getMovieDetail(@Path("movieId") movieId : Int, @Query("api_key") apiKey: String) : Observable<MovieDetailResponse>

}