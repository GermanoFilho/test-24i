package com.germanofilho.app.detail.presentation.presenter

import com.germanofilho.app.core.presenter.BasePresenter

interface DetailPresenter : BasePresenter {
    fun fetchMovieDetail(id: Int)
}