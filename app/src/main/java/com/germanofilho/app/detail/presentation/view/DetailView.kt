package com.germanofilho.app.detail.presentation.view

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import com.germanofilho.app.core.view.BaseView

interface DetailView : BaseView{
    fun loadMovieDetail(movieDetail : MovieDetailResponse)
    fun showError()
    fun showInvalidMovie()
    fun showMovieContent()
}