package com.germanofilho.app.detail.model.service

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import com.germanofilho.app.core.model.service.BaseService
import io.reactivex.Observable

interface DetailService : BaseService {
    fun getMovieDetail(movieId : Int) : Observable<MovieDetailResponse>
}