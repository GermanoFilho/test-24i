package com.germanofilho.app.detail.presentation.presenter

import android.util.Log
import com.germanofilho.app.core.presenter.BasePresenterImpl
import com.germanofilho.app.detail.model.usecase.DetailUseCase
import com.germanofilho.app.detail.presentation.view.DetailView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailPresenterImpl @Inject constructor(private val mView: DetailView) : BasePresenterImpl(mView), DetailPresenter {


    @Inject
    lateinit var mUseCase : DetailUseCase

    override fun fetchMovieDetail(id: Int) {
        mView.isToShowProgressBar(true)
        val observable = execute(mUseCase.getMovieDetail(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.isToShowProgressBar(false)
                    if(mUseCase.isValidMovie(it)){
                        mView.loadMovieDetail(it)
                        mView.showMovieContent()
                    } else {
                        mView.showInvalidMovie()
                    }

                    Log.i("Presenter", "success")

                }, {
                    mView.isToShowProgressBar(false)
                    mView.showError()
                    Log.e("Presenter", it.message)
                })


        mCompositeDisposable.add(observable)
    }
}