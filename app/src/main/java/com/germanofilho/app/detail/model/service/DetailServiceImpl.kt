package com.germanofilho.app.detail.model.service

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import com.germanofilho.app.core.model.service.BaseServiceConnector
import com.germanofilho.app.core.model.service.BaseServiceImpl
import io.reactivex.Observable
import javax.inject.Inject
class DetailServiceImpl @Inject constructor(var retrofit : BaseServiceConnector): BaseServiceImpl(), DetailService{
    override fun getMovieDetail(movieId : Int): Observable<MovieDetailResponse> = retrofit.request(getBaseUrl(), DetailServiceEndPoint::class.java).getMovieDetail(movieId, getKey())
}