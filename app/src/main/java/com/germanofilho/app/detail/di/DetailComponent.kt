package com.germanofilho.app.detail.di

import com.germanofilho.app.core.di.AppScope
import com.germanofilho.app.detail.presentation.view.ui.activity.DetailActivity
import com.germanofilho.app.movie.presentation.view.ui.activity.MovieActivity
import dagger.Subcomponent

@AppScope
@Subcomponent(modules = [DetailModule::class])
interface DetailComponent {
    fun inject(activity : DetailActivity)
}