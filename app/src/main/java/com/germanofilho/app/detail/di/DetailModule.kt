package com.germanofilho.app.detail.di

import com.germanofilho.app.core.di.AppScope
import com.germanofilho.app.core.view.BaseView
import com.germanofilho.app.detail.presentation.presenter.DetailPresenter
import com.germanofilho.app.detail.presentation.presenter.DetailPresenterImpl
import com.germanofilho.app.detail.presentation.view.DetailView
import com.germanofilho.app.movie.presentation.presenter.MoviePresenter
import com.germanofilho.app.movie.presentation.presenter.MoviePresenterImpl
import com.germanofilho.app.movie.presentation.view.MovieView
import dagger.Module
import dagger.Provides

@Module
class DetailModule constructor(var view: BaseView) {

    @Provides
    @AppScope
    fun provideDetailView(): DetailView = view as DetailView

    @Provides
    @AppScope
    fun provideDetailPresenter(presenter: DetailPresenterImpl): DetailPresenter = presenter

}