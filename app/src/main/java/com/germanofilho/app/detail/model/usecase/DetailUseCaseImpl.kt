package com.germanofilho.app.detail.model.usecase

import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import com.germanofilho.app.core.model.usecase.BaseUseCaseImpl
import com.germanofilho.app.detail.model.service.DetailService
import io.reactivex.Observable
import javax.inject.Inject

class DetailUseCaseImpl @Inject constructor(private var service : DetailService) : BaseUseCaseImpl(service), DetailUseCase{

    override fun getMovieDetail(movieId : Int): Observable<MovieDetailResponse> = execute(service.getMovieDetail(movieId))

    override fun isValidMovie(movie: MovieDetailResponse): Boolean {
        return !movie.overview.isNullOrEmpty() && !movie.genres.isNullOrEmpty() &&
                !movie.language.isNullOrEmpty() && !movie.releaseDate.isNullOrEmpty()
    }
}