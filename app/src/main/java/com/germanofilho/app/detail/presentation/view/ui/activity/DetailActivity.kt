package com.germanofilho.app.detail.presentation.view.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.germanofilho.app.App
import com.germanofilho.app.BuildConfig
import com.germanofilho.app.R
import com.germanofilho.app.core.model.domain.entity.MovieDetailResponse
import com.germanofilho.app.core.util.ScreenUtil
import com.germanofilho.app.core.view.ui.activity.BaseActivity
import com.germanofilho.app.detail.di.DetailModule
import com.germanofilho.app.detail.presentation.presenter.DetailPresenter
import com.germanofilho.app.detail.presentation.view.DetailView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.invalid_movie_layout.*
import kotlinx.android.synthetic.main.reload_layout.*
import kotlinx.android.synthetic.main.translucent_toolbar.*
import javax.inject.Inject

class DetailActivity : BaseActivity<DetailPresenter>(), DetailView {

    @Inject
    lateinit var mPresenter : DetailPresenter

    private var mId : Int? = 0

    companion object {
        fun newInstance(context: Context, id: Int): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(context.getString(R.string.param_id), id)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        isTablet()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        mId = intent.extras?.getInt(getString(R.string.param_id))

        App.get(this)
                .getAppComponent()
                .plus(DetailModule(this))
                .inject(this)

    }

    override fun onStart() {
        super.onStart()
        mPresenter.fetchMovieDetail(mId!!)

        reload_content.setOnClickListener {
            reload_content.visibility = View.GONE
            mPresenter.fetchMovieDetail(mId!!)
        }
    }

    override fun loadMovieDetail(movieDetail: MovieDetailResponse) {
        tv_title_movie.text = movieDetail.title
        tv_language_content.text = movieDetail.language?.toUpperCase()
        tv_release_date_content.text = movieDetail.releaseDate
        tv_overview_content.text = movieDetail.overview
        tv_genre_content.text = movieDetail.genres?.first()?.name
        Picasso.get().load(BuildConfig.BASE_IMAGE_URL.plus(ScreenUtil.getUrlTypePath()).plus(movieDetail.urlPoster)).into(imageview_movie)
    }

    override fun showError() {
        scrollview_about_movie.visibility = View.GONE
        reload_content.visibility = View.VISIBLE
    }

    override fun showMovieContent() {
        scrollview_about_movie.visibility = View.VISIBLE
        invalid_movie.visibility = View.GONE
        reload_content.visibility = View.GONE
    }

    override fun showInvalidMovie() {
        scrollview_about_movie.visibility = View.GONE
        invalid_movie.visibility = View.VISIBLE
        reload_content.visibility = View.GONE
    }
}

